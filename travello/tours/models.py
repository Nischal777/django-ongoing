from django.db import models
class Destination(models.Model):
    name = models.CharField(max_length= 100)
    pric = models.IntegerField(default = 0)
    descp = models.TextField()
    image = models.ImageField(upload_to ='pics')
    offer = models.BooleanField(default  = False)

# Create your models here.
