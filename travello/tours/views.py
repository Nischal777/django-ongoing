from django.shortcuts import render
from .models import Destination 
def index(request):
    dest1 = Destination()
    dest1.name ='kathmandu'
    dest1.price = 500
    dest1.descp = 'The city of pollution'
    dest1.image = 'destination_1.jpg'
    dest1.bool = True

    dest2 = Destination()
    dest2.name ='Lalitpur'
    dest2.price = 450
    dest2.descp = 'The city of handicraft'
    dest2.image = 'destination_2.jpg'
    dest2.bool = False

    dest3= Destination()
    dest3.name ='chitwan'
    dest3.price = 600
    dest3.descp = 'The forested area'
    dest3.image = 'destination_3.jpg'
    dest3.bool = True
    
    dests =[dest1, dest2, dest3]
    return  render(request, 'tours/index.html',{'dests':dests})
# Create your views here.
